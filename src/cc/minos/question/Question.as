package cc.minos.question
{
	import cc.minos.console.Console;
	import cc.minos.question.extensions.*;
	import cc.minos.utils.FilterUtil;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.filters.ColorMatrixFilter;
	import flash.utils.ByteArray;
	import flash.utils.getDefinitionByName;
	import cc.minos.question.plugins.*;
	
	/**
	 * ...
	 * 教育题目类
	 * 支持单选，多选，连线(1对1，多对1)，填空(有序，无序)，判断，拖动(多对1), 拖动(组句子), 播放声音
	 * 一道题可由各类型题目组成
	 * @author Minos
	 */
	public class Question extends Sprite implements IRecovery, IPrintData, IMarkup
	{
		//灰色滤镜
		public static const grayFilter:ColorMatrixFilter = new ColorMatrixFilter([ .33, .33, .33, 0, 0, .33, .33, .33, 0, 0, .33, .33, .33, 0, 0, 0, 0, 0, 1, 0 ] );
		//题目路径
		public static var QUESTION_PATH:String = "";
		//开启插件列表
		public static var activatedPlugins:Object = {};
		public static const acronyms:Object = { 'choice': 'ChoicePlugin', 'drag': 'DragPlugin', 'line': 'DrawLinePlugin', 'fillin': 'FillInPlugin', 'judgment': 'JudgmentPlugin', 'multi': 'MultiChoicePlugin', 'order': 'OrderFillInPlugin', 'sentence': 'SentencePlugin', 'single': 'SingleChoicePlugin', 'sound': 'SoundPlugin', 'view': 'ViewPlugin' }
		
		//数据
		protected var data:Object;
		//容器
		protected var container:DisplayObjectContainer;
		//资源
		private var _skin:Sprite;
		//插件列表
		protected var plugins:Array;
		//题目实例名
		protected var questionName:String = "q_$_mc";
		//答案剪辑
		protected var __answerMc:MovieClip;
		//是否已经检查
		protected var _isCheck:Boolean = false;
		
		public function Question()
		{
			//构造
		}
		
		/**
		 * 初始化
		 */
		protected function init():void
		{
			//组件数组
			plugins = [];
			//题目初始坐标
			skin.x = skin.y = 2;
			//题目组件数据
			var _ary:Array = data[ "data" ];
			//
			//循环根据组件创建代码
			for ( var i:int = 0, len:int = _ary.length; i < len; i++ )
			{
				var d:Object = _ary[ i ];
				var _class:Class;
				
				if ( d.type in activatedPlugins )
				{
					_class = activatedPlugins[ d.type ];
				}
				else if ( acronyms[ d.type ] in activatedPlugins )
				{
					_class = activatedPlugins[ acronyms[ d.type ] ];
				}
				else
				{
					Console.log( "Not Activated Plugin > " + "id: " + id + "type: " + d.type, this, 4 );
					return;
				}
				
				//获取插件
				var plugin:QuestionPlugin = new _class();
				//获取资源
				var _skin:DisplayObjectContainer = null;
				var _name:String = d.name ? d.name : d.localName ? d.localName : "";
				if ( skin.getChildByName( _name ) != null )
				{
					_skin = skin.getChildByName( _name ) as DisplayObjectContainer;
				}
				else if ( skin.getChildByName( questionName.replace( "$", _name ) ) != null )
				{
					_skin = skin.getChildByName( questionName.replace( "$", _name ) ) as DisplayObjectContainer;
				}
				else
				{
					_skin = skin;
				}
				//绑定数组和资源到插件
				plugin.bind( _skin, d );
				plugin.index = i;
				plugins[ i ] = plugin;
			}
			//获取答案剪辑
			__answerMc = skin.getChildByName( "answer_mc" ) as MovieClip;
		}
		
		/**
		 * 绑定
		 * @param	skin			:	题目
		 * @param	container		:	容器
		 * @param	data			:	数据
		 */
		public function bind( skin:Sprite, container:DisplayObjectContainer, data:Object, isInit:Boolean = true ):void
		{
			_skin = skin;
			this.container = container;
			this.data = data;
			if ( isInit )
				init();
		}
		
		/**
		 * 启用
		 * 添加题目到容器并启用题目的组件
		 */
		public function activate():void
		{
			if ( skin is DisplayObject )
				container.addChild( skin );
			if ( __answerMc && __answerMc.currentFrame == 2 )
				return;
			for each ( var i:QuestionPlugin in plugins )
			{
				i.activate()
			}
		}
		
		/**
		 * 取消
		 * 移除题目并取消组件
		 */
		public function cancel():void
		{
			if ( container.contains( skin ) )
				container.removeChild( skin );
			for each ( var i:QuestionPlugin in plugins )
			{
				i.cancel();
			}
		}
		
		/**
		 * 屏蔽
		 * 取消组件显示答案
		 */
		public function screen():Boolean
		{
			if ( __answerMc && __answerMc.currentFrame == 1 )
			{
				for each ( var i:QuestionPlugin in plugins )
				{
					i.cancel();
				}
				__answerMc.gotoAndStop( 2 );
				_isCheck = true;
				return false;
			}
			return true;
		}
		
		/**
		 * 重置题目
		 */
		public function reset( all:Boolean = true ):void
		{
			for each ( var i:QuestionPlugin in plugins )
			{
				i.reset( all );
			}
			_isCheck = false;
			if ( __answerMc )
				__answerMc.gotoAndStop( 1 );
		}
		
		/**
		 * 检查题目
		 * @return
		 */
		public function check():Boolean
		{
			var b:Boolean = true;
			for each ( var i:QuestionPlugin in plugins )
			{
				if ( !i.check() )
					b = false;
			}
			return b;
		}
		
		/**
		 * 获取题目分数
		 * @return
		 */
		public function get score():Number
		{
			var _socre:Number = 0;
			for each ( var i:QuestionPlugin in plugins )
			{
				_socre += i.score;
			}
			return _socre;
		}
		
		public function get totalScore():Number
		{
			return data[ "score" ] != undefined ? Number( data[ "score" ] ) : 0;
		}
		
		public function get isCheck():Boolean
		{
			return _isCheck;
		}
		
		public function get skin():Sprite
		{
			return _skin;
		}
		
		public function get point():String
		{
			return data[ "point" ] != undefined ? String( data[ "point" ] ) : data[ "checkPoint" ] != undefined ? String( data[ "checkPoint" ] ) : "";
		}
		
		public function get id():int
		{
			return data[ "id" ] != undefined ? int( data[ "id" ] ) : -1;
		}
		
		/* INTERFACE cc.minos.question.extensions.IPrintData */
		
		public function print():Object
		{
			return { point: point, id: id };
		}
		
		/* INTERFACE cc.minos.question.extensions.IMarkup */
		
		public function markup( mcs:Array = null ):void
		{
			for each ( var p:QuestionPlugin in plugins )
			{
				if ( p )
					p.markup( mcs );
			}
		}
		
		/* INTERFACE cc.minos.question.extensions.IRecovery */
		
		/**
		 * 備份題目數據
		 * 保存是否已經提交
		 * 各小題備份數據
		 * @return
		 */
		public function backup():Object
		{
			var qbackup:Object = {};
			qbackup.id = id;
			qbackup.isCheck = isCheck;
			qbackup.backups = [];
			for each ( var p:QuestionPlugin in plugins )
			{
				qbackup.backups.push( p.backup() );
			}
			return qbackup;
		}
		
		/**
		 * 恢復題目數據
		 * 恢復數據後會在外部檢測是否已經提交 然後判斷各小題對錯 如需標記則一起標記
		 * @param	value
		 */
		public function recover( value:Object ):void
		{
			if ( value == null && value.id != id )
			{
				Console.log( "恢復數據錯誤: " + value.id + " to " + id );
				return;
			}
			if ( value.backups != undefined )
			{
				var index:int = -1;
				for ( var o:Object in value.backups )
				{
					index = value.backups[ o ].index;
					var qp:QuestionPlugin = plugins[ index ];
					qp.recover( value.backups[ o ] );
				}
			}
			if ( value.isCheck != undefined )
			{
				_isCheck = value.isCheck;
			}
		}
	}

}