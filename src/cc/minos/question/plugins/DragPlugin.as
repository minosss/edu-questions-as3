package cc.minos.question.plugins
{
	import cc.minos.console.Console;
	import cc.minos.question.extensions.Mark;
	import cc.minos.utils.ArrayUtil;
	import cc.minos.utils.StringUtil;
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.IEventDispatcher;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	
	/**
	 * ...
	 * 拖动题
	 * @author Minos
	 */
	public class DragPlugin extends ChoicePlugin
	{
		public static const API:Number = 2.1;
		public static const NAME:String = "[DragPlugin]";
		
		protected var hits:Array;
		protected var hitedAry:Array;
		protected var points:Array;
		protected var currentChoice:MovieClip;
		
		private var hreg:RegExp = /\bh_[0-9]{1,2}_mc\b/;
		
		public function DragPlugin()
		{
			this.propName = "DragPlugin";
		}
		
		/**
		 * 初始化拖動題
		 */
		override protected function init():void
		{
			super.init();
			hits = [];
			points = [];
			hitedAry = [];
			
			for each ( var o:Object in skin )
			{
				if ( o && o is Sprite && hreg.test( o.name ) )
				{
					hits[ getIndex( o.name ) ] = o;
				}
			}
			
			//save choices default position
			for ( var i:int = 0; i < options; i++ )
			{
				points.push( new Point( choices[ i ].x, choices[ i ].y ) );
			}
		
		}
		
		/**
		 *
		 * @param	s
		 * @return
		 */
		private function getTextFiled( s:Sprite ):TextField
		{
			return ( s.getChildByName( 'txt' ) as TextField );
		}
		
		/**
		 * 拖動釋放
		 * @param	e
		 */
		private function onChoicesUp( e:MouseEvent ):void
		{
			var index:int = getIndex( currentChoice.name );
			currentChoice.stopDrag();
			currentChoice.stage.removeEventListener( MouseEvent.MOUSE_UP, onChoicesUp );
			//check hit
			for each ( var i:DisplayObject in hits )
			{
				if ( i.hitTestObject( currentChoice ) )
				{
					hitedAry[ getIndex( i.name ) ] = index;
					break;
				}
			}
			currentChoice = null;
			
			fillHited();
		}
		
		/**
		 * 根據hited數據
		 * 更新選項位置
		 */
		private function fillHited():void
		{
			for ( var j:int = 0; j < choices.length; j++ )
			{
				var m:int = hitedAry.indexOf( getIndex( choices[ j ].name ) );
				if ( m != -1 )
				{
					choices[ j ].x = hits[ m ].x;
					choices[ j ].y = hits[ m ].y;
				}
				else
				{
					choices[ j ].x = points[ j ].x;
					choices[ j ].y = points[ j ].y;
				}
			}
		}
		
		/**
		 * 更新狀態
		 * @param	mc
		 */
		override protected function update( mc:MovieClip = null ):void
		{
			super.update( mc );
			mc.stage.addEventListener( MouseEvent.MOUSE_UP, onChoicesUp );
			mc.startDrag();
			skin.addChild( mc );
			currentChoice = mc;
			if ( hitedAry.indexOf( getIndex( currentChoice.name ) ) != -1 )
			{
				hitedAry[ hitedAry.indexOf( getIndex( currentChoice.name ) ) ] = null;
			}
		}
		
		/**
		 * 判斷題目是否正確
		 * 拖動題判斷全對
		 * @return
		 */
		override public function check():Boolean
		{
			for ( var i:int = 0, len:int = answer.length; i < len; i++ )
			{
				if ( hitedAry[ i ] == null || !_check( i ) )
				{
					return false;
				}
			}
			return true;
		}
		
		/**
		 *
		 * @param	all
		 */
		override public function reset( all:Boolean = true ):void
		{
			super.reset( all );
			hitedAry = [];
			for ( var i:int = 0, len:int = choices.length; i < len; i++ )
			{
				choices[ i ].x = points[ i ].x;
				choices[ i ].y = points[ i ].y;
			}
		}
		
		/**
		 * 根据答案类型检查
		 * @param	i
		 */
		protected function _check( i:int ):Boolean
		{
			var txt:String = choiceSigns[ hitedAry[ i ] ];
			if ( txt == null )
				return false;
			var ans:Object = answer[ i ];
			var b:Boolean = true;
			txt = StringUtil.trim( txt );
			
			if ( ans is Array )
			{
				if (( ans as Array ).indexOf( txt ) == -1 )
					b = false;
			}
			else
			{
				if ( StringUtil.trim( ans.toString() ) != txt )
					b = false;
			}
			return b;
		}
		
		/**
		 * 得分
		 */
		override public function get score():Number
		{
			_score = 0;
			if ( checkAll )
			{
				return check() ? tScore : 0;
			}
			
			for ( var i:int = 0, len:int = answer.length; i < len; i++ )
			{
				if ( hitedAry[ i ] != null && _check( i ) )
				{
					_score += tScore / len;
				}
				else
				{
					if ( hitedAry[ i ] != null )
						Console.log( '答案错误: ' + choiceSigns[ hitedAry[ i ] ] + ' - ' + answer[ i ], NAME, 1 );
				}
			}
			
			return _score;
		}
		
		/**
		 * 標記
		 * @param	mcs
		 */
		override public function markup( mcs:Array = null ):void
		{
			if ( mcs != null )
			{
				markups.length = 0;
				var mark:Mark;
				var rect:Rectangle;
				for ( var i:int = 0; i < hits.length; i++ )
				{
					rect = hits[i].getRect(skin);
					mark = new Mark();
					mark.x = rect.x;
					mark.y = rect.y;
					mark.width = rect.width;
					mark.height = rect.height;
					mark.position = Mark.RIGHT;
					mark.type = _check( i ) ? 0 : 1;
					markups.push( mark );
				}
			}
			super.markup( mcs );
		}
		
		/**
		 * 備份
		 * @return
		 */
		override public function backup():Object
		{
			var bu:Object = {};
			bu.index = index;
			bu.hitedAry = hitedAry;
			return bu;
		}
		
		/**
		 * 恢復
		 *
		 * @param	value
		 */
		override public function recover( value:Object ):void
		{
			if ( value == null )
				return;
			if ( value.hitedAry != undefined )
			{
				/*hitedAry.length = 0;
				   var ary:Array = String( value.hitedAry ).split( "," );
				   for ( var i:int = 0; i < ary.length; i++ )
				   {
				   hitedAry.push( int( ary[ i ] ) );
				 }*/
				hitedAry = value.hitedAry;
				fillHited();
			}
		}
	}

}