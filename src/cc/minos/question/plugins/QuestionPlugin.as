package cc.minos.question.plugins
{
	import cc.minos.question.extensions.*;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.utils.ByteArray;
	import cc.minos.question.Question;
	
	/**
	 * ...
	 * 题目插件基类
	 * @author Minos
	 */
	public class QuestionPlugin implements IRecovery, IPrintData, IMarkup
	{
		//API
		public static const API:Number = 2.1;
		//插件类型
		public var propName:String;
		//public var acronym:String;
		protected var _index:int = -1;
		//素材
		protected var skin:DisplayObjectContainer;
		//数据
		protected var data:Object;
		//分数
		protected var _tScore:Number;
		//得分
		protected var _score:Number;
		//检查全部
		protected var checkAll:Boolean;
		//答案
		protected var answer:Array;
		//选项标识符
		protected var optionName:String = "a_$_obj";
		protected var options:Number = 0;
		
		protected var markContainer:Sprite;
		
		public function QuestionPlugin()
		{
			//构造
		}
		
		/**
		 * 初始化插件
		 */
		protected function init():void
		{
			_tScore = Number( getData( "score" ) );
			var s:String = getData( "all" ) ? String( getData( "all" ) ) : String( getData( "checkAll" ) );
			checkAll = s.toUpperCase() == "TRUE" ? true : false;
			answer = getAnswer();
			//myAnswer;
			_score = 0;
			markContainer = new Sprite();
			markContainer.mouseChildren = false;
			skin.addChild( markContainer );
		}
		
		/**
		 * 获取参数
		 * @param	v	:	参数
		 * @return
		 */
		protected final function getData( v:String ):Object
		{
			if ( data[ v ] )
				return data[ v ];
			return null;
		}
		
		/**
		 * 检查是否有该参数
		 * @param	v	:	参数
		 * @return
		 */
		protected final function hasData( v:String ):Boolean
		{
			return data[ v ] ? true : false;
		}
		
		/**
		 * 获取素材中的可视对象
		 * @param	name
		 * @return
		 */
		protected final function getChild( name:String ):DisplayObject
		{
			var d:DisplayObject = null;
			if ( skin && skin is DisplayObjectContainer )
				d = skin.getChildByName( name );
			return d;
		}
		
		/**
		 * 获取答案数组（深复制）
		 * @return
		 */
		protected final function getAnswer():Array
		{
			var ba:ByteArray = new ByteArray();
			ba.writeObject( getData( "answer" ) );
			ba.position = 0;
			return ba.readObject() as Array;
		}
		
		/**
		 * 绑定素材和数据
		 * @param	skin
		 * @param	data
		 */
		public function bind( skin:DisplayObjectContainer, data:Object = null ):void
		{
			this.data = data;
			this.skin = skin;
			init();
		}
		
		/**
		 * 启动
		 */
		public function activate():void
		{
			addListeners();
		}
		
		/**
		 * 取消
		 */
		public function cancel():void
		{
			removeListeners();
		}
		
		/**
		 * 重置
		 */
		public function reset( all:Boolean = true ):void
		{
			//to protected
			markup( null );
		}
		
		/**
		 * 添加侦听
		 */
		protected function addListeners():void
		{
			//to protected
		}
		
		/**
		 * 移除侦听
		 */
		protected function removeListeners():void
		{
			//to protected
		}
		
		/**
		 * 检查
		 * @return
		 */
		public function check():Boolean
		{
			return false;
		}
		
		/**
		 * 获取得分
		 */
		public function get score():Number
		{
			return _score;
		}
		
		public function get tScore():Number
		{
			return _tScore;
		}
		
		/**
		 * 序號
		 */
		public function get index():int
		{
			return _index;
		}
		
		public function set index( value:int ):void
		{
			_index = value;
		}
		
		/**
		 * 启动插件
		 * @param	plugins
		 * @return
		 */
		public static function activate( plugins:Array ):Boolean
		{
			var i:int = plugins.length;
			while ( --i > -1 )
			{
				if ( plugins[ i ].API == QuestionPlugin.API )
				{
					Question.activatedPlugins[( new ( plugins[ i ] as Class )() ).propName ] = plugins[ i ];
				}
			}
			return true
		}
		
		/* 備份 INTERFACE cc.minos.question.extensions.IRecovery */
		
		public function backup():Object
		{
			return null;
		}
		
		public function recover( value:Object ):void
		{
		}
		
		/* INTERFACE cc.minos.question.extensions.IPrintData */
		
		public function print():Object
		{
			var p:Object = {};
			p.options = 0;
			return p;
		}
		
		/* INTERFACE cc.minos.question.extensions.IMarkup */
		
		protected var markups:Array = [];
		
		public function markup( mcs:Array = null ):void
		{
			while ( markContainer.numChildren > 0 )
			{
				markContainer.removeChildAt( 0 );
			}
			if ( mcs == null )
			{
			}
			else
			{
				skin.addChild( markContainer );
				var mark:Mark;
				var xpos:int = 0, ypos:int = 0;
				for ( var j:int = 0; j < markups.length; j++ )
				{
					mark = markups[ j ];
					if ( mark.type >= mcs.length )
						continue;
					var bmp:Bitmap = new Bitmap();
					bmp.bitmapData = mcs[ mark.type ];
					switch ( mark.position )
					{
						case Mark.UP: 
							xpos = mark.width - bmp.width >> 1;
							//ypos = -bmp.height;
							break;
						case Mark.DOWN: 
							xpos = mark.width - bmp.width >> 1;
							ypos = mark.height - bmp.height;
							break;
						case Mark.LEFT: 
							//xpos = -bmp.width;
							ypos = mark.height - bmp.height >> 1;
							break;
						case Mark.RIGHT: 
							xpos = mark.width-bmp.width;
							ypos = mark.height - bmp.height >> 1;
							break;
						case Mark.CENTER: 
							xpos = mark.width - bmp.width >> 1;
							ypos = mark.height - bmp.height >> 1;
							break;
						case Mark.LEFT_UP: 
							//xpos = -bmp.width * .5;
							//ypos = -bmp.height * .5;
							break;
						case Mark.LEFT_DOWN: 
							//xpos = -bmp.width * .5;
							ypos = mark.height - bmp.height;
							break;
						case Mark.RIGHT_UP: 
							xpos = mark.width - bmp.width;
							//ypos = -bmp.height * .5;
							break;
						case Mark.RIGHT_DOWN: 
							xpos = mark.width - bmp.width;
							ypos = mark.height - bmp.height;
							break;
					}
					bmp.x = mark.x + xpos;
					bmp.y = mark.y + ypos;
					markContainer.addChild( bmp );
				}
			}
		}
	
	}

}