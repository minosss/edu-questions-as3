package cc.minos.question.plugins
{
	import cc.minos.utils.StringUtil;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.IEventDispatcher;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	
	/**
	 * ...
	 * Choice 点击
	 * 点击题型都会继承此类
	 * @author Minos
	 */
	public class ChoicePlugin extends QuestionPlugin
	{
		public static const API:Number = 2.1;
		//
		protected var choices:Array;
		//save choiced mc
		protected var choicedAry:Array;
		//choice sign eg: ["ABCD"]
		protected var choiceSigns:Array;
		
		private var creg:RegExp = /\ba_[0-9]{1,2}_mc\b/;
		
		public function ChoicePlugin()
		{
			this.propName = "ChoicePlugin";
			//this.acronym = "choice";
			optionName = "a_$_mc";
		}
		
		// Protected Methods //
		
		override protected function init():void
		{
			super.init();
			choices = [];
			choicedAry = [];
			
			for each ( var o:Object in skin )
			{
				if ( o && o is Sprite && creg.test( o.name ) )
				{
					o.buttonMode = true;
					choices[ getIndex( o.name ) ] = o;
				}
			}
			options = choices.length;
			choiceSigns = getSings();
		}
		
		protected function getIndex( n:String ):int
		{
			return int( StringUtil.substringBySign( n ) );
		}
		
		/**
		 *
		 * @return
		 */
		private function getSings():Array
		{
			var str:String = getData( "choiceSign" ) ? String( getData( "choiceSign" ) ) : getData( "signs" ) ? String( getData( "signs" ) ) : "";
			if ( str.indexOf( "|" ) != -1 )
				return str.split( "|" );
			if ( str.indexOf( "," ) != -1 )
				return str.split( "," );
			return str.split( "" );
		}
		
		override protected function addListeners():void
		{
			super.addListeners();
			//
			for each ( var c:IEventDispatcher in choices )
			{
				if ( c )
				{
					c.addEventListener( MouseEvent.MOUSE_DOWN, onChoicesDown );
					c.addEventListener( MouseEvent.ROLL_OUT, onChoicesOut );
					c.addEventListener( MouseEvent.ROLL_OVER, onChoicesOver );
				}
			}
		}
		
		override protected function removeListeners():void
		{
			super.removeListeners();
			for each ( var c:IEventDispatcher in choices )
			{
				if ( c )
				{
					c.removeEventListener( MouseEvent.MOUSE_DOWN, onChoicesDown );
					c.removeEventListener( MouseEvent.ROLL_OUT, onChoicesOut );
					c.removeEventListener( MouseEvent.ROLL_OVER, onChoicesOver );
				}
			}
		}
		
		/**
		 * update choice array's mc status
		 * @param	mc
		 */
		protected function update( mc:MovieClip = null ):void
		{
			//to protected
			if ( !mc )
				return;
		}
		
		// Event Methods //
		
		private function onChoicesOut( e:MouseEvent ):void
		{
			var mc:MovieClip = e.currentTarget as MovieClip;
			if ( mc && choicedAry.indexOf( mc ) == -1 )
			{
				mc.gotoAndStop( 1 );
			}
		}
		
		private function onChoicesOver( e:MouseEvent ):void
		{
			var mc:MovieClip = e.currentTarget as MovieClip;
			if ( mc && choicedAry.indexOf( mc ) == -1 )
			{
				mc.gotoAndStop( 2 );
			}
		}
		
		private function onChoicesDown( e:MouseEvent ):void
		{
			var mc:MovieClip = e.currentTarget as MovieClip;
			if ( mc )
			{
				update( mc );
				mc.gotoAndStop( choicedAry.indexOf( mc ) == -1 ? 2 : 3 );
			}
		}
		
		// Public Method //
		
		override public function reset( all:Boolean = true ):void
		{
			super.reset( all );
			for each ( var c:MovieClip in choices )
			{
				c.gotoAndStop( 1 );
			}
		}
	}

}