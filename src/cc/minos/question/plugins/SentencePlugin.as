package cc.minos.question.plugins
{
	import cc.minos.console.Console;
	import cc.minos.question.extensions.Mark;
	import cc.minos.utils.ArrayUtil;
	import cc.minos.utils.StringUtil;
	import com.greensock.motionPaths.MotionPath;
	import flash.display.InteractiveObject;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	
	/**
	 * ...
	 * 组成句子
	 * @author Minos
	 */
	public class SentencePlugin extends ChoicePlugin
	{
		public static const NAME:String = "[SentencePlugin]";
		public static const API:Number = 2.1;
		
		protected var hmc:Sprite;
		private var hited:Array;
		private var tfs:Array;
		protected var points:Array;
		protected var currentChoice:MovieClip;
		
		public function SentencePlugin()
		{
			this.propName = "SentencePlugin";
		}
		
		override protected function init():void
		{
			super.init();
			points = [];
			hited = [];
			tfs = [];
			
			//get hits
			hmc = skin.getChildByName( "h_mc" ) as Sprite;
			if ( hmc != null )
			{
				hmc.alpha = 0;
			}
			else
			{
				Console.log( 'hit movieclip not found!', this, 2 );
			}
			//save choices default position
			if ( choices.length == 0 )
			{
				Console.log( "not found choices, plase check movieclip name.", this, Console.ERROR );
				return;
			}
			var tf:TextField;
			var xpos:Number = choices[ 0 ].x;
			var ypos:Number = choices[ 0 ].y;
			for ( var i:int = 0; i < options; i++ )
			{
				choices[ i ].x = xpos;
				choices[ i ].y = ypos;
				tf = getTextFiled( choices[ i ] );
				if ( tf )
				{
					if ( choiceSigns[ i ] != null )
					{
						tf.text = choiceSigns[ i ];
						tfs[ i ] = tf;
					}
					else
					{
						tf.visible = false;
					}
					tf.autoSize = "left";
					tf.selectable = false;
				}
				xpos += ( choices[ i ].width + 10 );
				points.push( new Point( choices[ i ].x, choices[ i ].y ) );
			}
		
		}
		
		//2012-09-26
		private var addto:int = -1;
		
		private function onChoicesMove( e:MouseEvent ):void
		{
			if ( !currentChoice || !currentChoice.hitTestObject( hmc ) )
				return;
			
			var xpos:Number = hmc.x;
			var b:Boolean = true;
			for ( var i:int = 0, len:int = hited.length; i < len; i++ )
			{
				if ( b && hited[ i ].x > currentChoice.x )
				{
					xpos += currentChoice.width + 2;
					b = false;
				}
				hited[ i ].x = xpos;
				xpos += hited[ i ].width + 2;
			}
		}
		
		private function onChoicesUp( e:MouseEvent ):void
		{
			//super.onChoicesUp(e);
			var index:int = getIndex( currentChoice.name );
			currentChoice.stopDrag();
			currentChoice.stage.removeEventListener( MouseEvent.MOUSE_UP, onChoicesUp );
			currentChoice.stage.removeEventListener( MouseEvent.MOUSE_MOVE, onChoicesMove );
			//
			if ( hmc.hitTestObject( currentChoice ) )
			{
				//set choice position
				if ( !ArrayUtil.containsValue( hited, currentChoice ) )
				{
					addto = -1;
					for ( var i:int = 0, len:int = hited.length; i < len; i++ )
					{
						if ( hited[ i ].x > currentChoice.x )
						{
							addto = i;
							break;
						}
					}
					
					if ( addto == -1 )
					{
						hited.push( currentChoice );
					}
					else
					{
						hited.splice( addto, 0, currentChoice );
					}
					
				}
				updateHits();
				return;
			}
			else
			{
				if ( ArrayUtil.containsValue( hited, currentChoice ) )
				{
					ArrayUtil.removeValue( hited, currentChoice );
				}
				updateHits();
			}
			//hited.
			//
			currentChoice.x = points[ index ].x;
			currentChoice.y = points[ index ].y;
			toLowerCase( currentChoice );
			currentChoice = null;
		}
		
		private function getTextFiled( s:Sprite ):TextField
		{
			try
			{
				return ( s.getChildByName( 'txt' ) as TextField );
			}
			catch ( ex:Error )
			{
				Console.log( s.name + ' textfiled not found! ', this, 3 );
			}
			return new TextField();
		}
		
		private function updateHits():void
		{
			var xpos:Number = hmc.x;
			for ( var i:int = 0, len:int = hited.length; i < len; i++ )
			{
				if ( i == 0 )
				{
					toUpperCase( hited[ i ] );
				}
				else
				{
					toLowerCase( hited[ i ] );
				}
				
				hited[ i ].x = xpos;
				hited[ i ].y = hmc.y;
				xpos += hited[ i ].width + 2;
			}
		}
		
		private function toUpperCase( choice:Sprite ):void
		{
			if ( !getTextFiled( choice ) )
				return;
			var str:String = getTextFiled( choice ).text;
			getTextFiled( choice ).text = str.replace( str.charAt(), str.charAt().toUpperCase() );
		}
		
		private function toLowerCase( choice:Sprite ):void
		{
			if ( !getTextFiled( choice ) )
				return;
			//var str:String = getTextFiled( choice ).text;
			//getTextFiled( choice ).text = str.replace( str.charAt() , str.charAt().toLowerCase() );
			
			//0920 reset choice's textfiled by the signs
			var index:int = getIndex( choice.name );
			getTextFiled( choice ).text = choiceSigns[ index ] == null ? '' : choiceSigns[ index ];
		}
		
		override protected function update( mc:MovieClip = null ):void
		{
			super.update( mc );
			mc.stage.addEventListener( MouseEvent.MOUSE_UP, onChoicesUp );
			mc.stage.addEventListener( MouseEvent.MOUSE_MOVE, onChoicesMove );
			mc.startDrag();
			skin.addChild( mc );
			currentChoice = mc;
			
			addto = -1;
			ArrayUtil.removeValue( hited, currentChoice );
		}
		
		//add 2012/12/21
		private function _check( i:int ):Boolean
		{
			var txt:String = getTextFiled( hited[ i ] ).text.toLowerCase();
			var ans:Object = answer[ i ];
			
			if ( ans is Array )
			{
				for ( var j:int = 0, len:int = ans.length; j < len; j++ )
				{
					if ( StringUtil.trim( ans[ j ].toString().toLowerCase() ) == txt )
						return true;
				}
				return false;
			}
			else
			{
				if ( StringUtil.trim( ans.toString().toLowerCase() ) != txt )
					return false;
			}
			return true;
		}
		
		override public function check():Boolean
		{
			for ( var i:int = 0, len:int = answer.length; i < len; i++ )
			{
				//if ( hited[ i ] == null || getTextFiled( hited[ i ] ).text.toLowerCase() != answer[ i ].toLowerCase() )
				if ( hited[ i ] == null || !_check( i ) )
				{
					/*if ( hited[ i ]!=null && getTextFiled( hited[ i ] ).text.toLowerCase() != answer[ i ].toLowerCase() )
					   {
					   Console.log( '答案错误: ' + getTextFiled( hited[ i ] ).text.toLowerCase() + ' - ' + answer[ i ].toLowerCase() , this , 1 );
					 }*/
					return false;
				}
			}
			return true;
		}
		
		override public function reset( all:Boolean = true ):void
		{
			super.reset( all );
			//reposition choice button
			for ( var i:int = 0, len:int = choices.length; i < len; i++ )
			{
				choices[ i ].x = points[ i ].x;
				choices[ i ].y = points[ i ].y;
				toLowerCase( choices[ i ] );
			}
			hited = [];
		}
		
		override public function get score():Number
		{
			return check() ? tScore : 0;
		}
		
		override public function markup( mcs:Array = null ):void
		{
			if ( mcs != null )
			{
				markups.length = 0;
				var mark:Mark = new Mark();
				var rect:Rectangle = hmc.getRect( skin );
				mark.x = rect.x;
				mark.y = rect.y;
				mark.width = rect.width;
				mark.height = rect.height;
				mark.position = Mark.RIGHT;
				mark.type = check() ? 0 : 1;
				markups.push( mark );
			}
			super.markup( mcs );
		}
		
		/**
		 * 拖動組句子
		 * 保存在句子中的詞語
		 * @return
		 */
		override public function backup():Object
		{
			var bu:Object = { };
			bu.index = index;
			bu.hited = [];
			for ( var i:int = 0; i < hited.length; i++)
			{
				bu.hited[i] = getIndex(hited[i].name);
			}
			return bu;
		}
		
		override public function recover( value:Object ):void
		{
			if ( value == null )
				return;
			
			if ( value.hited != undefined )
			{
				var array:Array = value.hited;
				for ( var i:int = 0; i < array.length; i++)
				{
					hited[i] = choices[array[i]];
				}
				updateHits();
			}
		}
	}

}