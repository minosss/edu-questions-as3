package cc.minos.question.plugins
{
	import cc.minos.console.Console;
	import com.greensock.events.LoaderEvent;
	import com.greensock.loading.MP3Loader;
	import flash.display.DisplayObject;
	import flash.display.Shape;
	import flash.display.SimpleButton;
	import flash.events.MouseEvent;
	import flash.media.Sound;
	import flash.utils.setTimeout;
	import cc.minos.question.Question;
	
	/**
	 * ...
	 * play sound plugin
	 * @author Minos
	 */
	public class SoundPlugin extends QuestionPlugin
	{
		public static const API:Number = 2.1;
		public static const NAME:String = "[SoundPlugin]";
		//
		private static var _soundTip:Sound;
		//play sound button
		protected var __playBtn:SimpleButton;
		//sound loader
		protected var sound:MP3Loader;
		//sound url
		protected var url:String;
		//repeat times
		protected var repeat:int;
		protected var time:int;
		//
		protected var isClick:Boolean = false;
		
		private var isPlay:Boolean;
		
		private var _pauseNum:int = -1;
		
		public function SoundPlugin()
		{
			this.propName = "SoundPlugin";
		}
		
		// ---------------------------------- Protected Methods ---------------------------------- //
		
		override protected function init():void
		{
			super.init();
			__playBtn = getChild( "play_btn" ) as SimpleButton;
			
			if ( !__playBtn )
			{
				__playBtn = new SimpleButton();
				Console.log( "play_btn播放按钮没找到.", NAME, 2 );
			}
			repeat = getData( "repeat" ) as int;
			time = getData( "time" ) as int;
			url = Question.QUESTION_PATH + getData( "url" ) as String;
			
			sound = new MP3Loader( url, { onComplete: onComplete, onError: onError, autoPlay: false } );
			sound.addEventListener( MP3Loader.SOUND_COMPLETE, onSoundComplete );
			sound.load();
		
		}
		
		override protected function addListeners():void
		{
			super.addListeners();
			__playBtn.addEventListener( MouseEvent.MOUSE_UP, onPlayHandler );
		}
		
		override protected function removeListeners():void
		{
			super.removeListeners();
			__playBtn.removeEventListener( MouseEvent.MOUSE_UP, onPlayHandler );
		}
		
		// ---------------------------------- Public Methods ---------------------------------- //
		
		override public function activate():void
		{
			super.activate();
			sound.gotoSoundTime( 0 );
			if ( !isClick )
				reset();
			isPlay = true;
		}
		
		override public function cancel():void
		{
			super.cancel();
			sound.pauseSound();
			__playBtn.filters = [ Question.grayFilter ];
			__playBtn.mouseEnabled = false;
			isPlay = false;
		}
		
		override public function reset( all:Boolean = true ):void
		{
			super.reset( all );
			repeat = getData( "repeat" ) as int;
			isClick = false;
			__playBtn.filters = [];
			__playBtn.mouseEnabled = true;
		}
		
		override public function check():Boolean
		{
			return true;
		}
		
		// ---------------------------------- Event Methods ---------------------------------- //
		
		/**
		 * sound loader complete
		 * @param	e
		 */
		private function onComplete( e:LoaderEvent ):void
		{
			sound.removeEventListener( LoaderEvent.COMPLETE, onComplete );
			sound.removeEventListener( LoaderEvent.ERROR, onError );
		}
		
		/**
		 * sound loader error
		 * @param	e
		 */
		private function onError( e:LoaderEvent ):void
		{
			sound.removeEventListener( LoaderEvent.COMPLETE, onComplete );
			sound.removeEventListener( LoaderEvent.ERROR, onError );
		}
		
		/**
		 * play button click
		 * @param	e
		 */
		private function onPlayHandler( e:MouseEvent ):void
		{
			isClick = true;
			
			if ( sound.soundPaused )
				sound.playSound();
			else
				sound.pauseSound();
			
			if ( pauseNum < 0 )
			{
				removeListeners();
				__playBtn.mouseEnabled = false;
				__playBtn.filters = [ Question.grayFilter ];
			}
		}
		
		/**
		 * sound play complete
		 * @param	e
		 */
		private function onSoundComplete( e:LoaderEvent ):void
		{
			repeat--;
			if ( repeat > 0 )
			{
				setTimeout( function():void
					{
						if ( isPlay )
							sound.gotoSoundTime( time, true );
					}, 2000 );
			}
			
			if ( _soundTip )
			{
				_soundTip.play( 0 );
			}
		}
		
		public function get pauseNum():int
		{
			return _pauseNum;
		}
		
		public function set pauseNum( value:int ):void
		{
			_pauseNum = value;
		}
		
		static public function set soundTip( value:Sound ):void
		{
			_soundTip = value;
		}
		
		static public function get soundTip():Sound
		{
			return _soundTip;
		}
		
		/**
		 * 備份
		 * @return
		 */
		override public function backup():Object
		{
			var bu:Object = {};
			bu.index = index;
			//bu.isClick = isClick;
			bu.repeat = repeat;
			bu.enabled = __playBtn.mouseEnabled;
			return bu;
		}
		
		/**
		 * 恢復
		 * 傳入一個對象恢復按鈕插件
		 * 是否已經點擊過按鈕isClick
		 * 重複次數repeat
		 * 按鈕是否啟用enabled
		 * @param	value
		 */
		override public function recover( value:Object ):void
		{
			if ( value == null )
				return;
			
			if ( value.repeat != undefined )
				repeat = value.repeat;
			
			if ( value.enabled != undefined )
			{
				isClick = !value.enabled;
				__playBtn.mouseEnabled = value.enabled;
				__playBtn.filters = __playBtn.mouseEnabled ? [] : [ Question.grayFilter ];
			}
		
		}
	}

}