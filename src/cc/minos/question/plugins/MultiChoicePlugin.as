package cc.minos.question.plugins
{
	
	/**
	 * ...
	 * 多选题
	 * @author Minos
	 */
	public class MultiChoicePlugin extends SingleChoicePlugin
	{
		public static const NAME:String = "[MultiChoicePlugin]";
		public static const API:Number = 2.1;
		
		public function MultiChoicePlugin()
		{
			super();
			this.propName = "MultiChoicePlugin";
			isSingle = false;
		}
	
	}

}