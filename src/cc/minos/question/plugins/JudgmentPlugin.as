package cc.minos.question.plugins
{
	import cc.minos.question.extensions.Mark;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	
	/**
	 * ...
	 * 判断
	 * @author Minos
	 */
	public class JudgmentPlugin extends SingleChoicePlugin
	{
		public static const NAME:String = "[JudgmentPlugin]";
		public static const API:Number = 2.1;
		
		protected var txtName:String = "a_$_txt"
		
		public function JudgmentPlugin()
		{
			this.propName = "JudgmentPlugin";
		}
		
		override protected function init():void
		{
			super.init();
			for ( var i:int = 0; i < options; i++ )
			{
				var n:String = txtName.replace( "$", i );
				var t:TextField = getChild( n ) as TextField;
				if ( t )
					t.text = choiceSigns[ i ];
			}
		}
		
		override public function check():Boolean
		{
			return choiceTxt == answer[ 0 ];
		}
		
		override public function markup( mcs:Array = null ):void
		{
			if ( mcs != null )
			{
				markups.length = 0;
				var mark:Mark = new Mark();
				var rect:Rectangle = choices[ 0 ].getRect( skin );
				mark.x = rect.x;
				mark.y = rect.y;
				mark.width = rect.width;
				mark.height = rect.height;
				mark.position = Mark.LEFT;
				mark.type = check() ? 0 : 1;
				markups.push( mark );
			}
			super.markup( mcs );
		}
	
	}

}