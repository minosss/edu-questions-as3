package cc.minos.question.plugins
{
	
	/**
	 * ...
	 * 主观题（取消）
	 * @author Minos
	 */
	public class ViewPlugin extends FillInPlugin
	{
		public static const VERSION:Number = 1.0;
		public static const API:Number = 1.0;
		
		public function ViewPlugin()
		{
			this.propName = "ViewPlugin";
		}
		
		override public function check():Boolean
		{
			return false;
		}
		
		override public function get score():Number
		{
			return 0;
		}
	
	}

}