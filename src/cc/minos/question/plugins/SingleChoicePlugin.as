package cc.minos.question.plugins
{
	import cc.minos.console.Console;
	import cc.minos.question.extensions.Mark;
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	
	/**
	 * ...
	 * SingleChoice 单选题
	 * @author Minos
	 */
	public class SingleChoicePlugin extends ChoicePlugin
	{
		public static const NAME:String = "[SingleChoicePlugin]";
		public static const API:Number = 2.1;
		//show selected textfield
		protected var choiceTf:TextField;
		//single or multi
		protected var isSingle:Boolean = true;
		//selected string
		protected var choiceTxt:String;
		
		public function SingleChoicePlugin()
		{
			this.propName = "SingleChoicePlugin";
		}
		
		// ---------------------------------- Protected Methods ---------------------------------- //
		
		/**
		 * get assets
		 * initialize plugin
		 */
		override protected function init():void
		{
			super.init();
			choiceTxt = "";
			choiceTf = getChild( "txt" ) as TextField;
			if ( choiceTf )
			{
				choiceTf.type = TextFieldType.DYNAMIC;
				choiceTf.selectable = false;
			}
			else if ( this.propName != "JudgmentPlugin" )
				Console.log( 'choice textfiled on found.', this, Console.WARNING );
		}
		
		/**
		 * update choiced array && choiced tf
		 * @param	mc
		 */
		override protected function update( mc:MovieClip = null ):void
		{
			super.update( mc );
			if ( !mc )
				return;
			var index:int = isSingle ? 0 : getIndex( mc.name );
			
			if ( !choicedAry[ index ] )
				choicedAry[ index ] = mc;
			else if ( choicedAry[ index ] != mc )
			{
				choicedAry[ index ].gotoAndStop( 1 );
				choicedAry[ index ] = mc;
			}
			else {
				//choicedAry[ index ] = null;
				choicedAry.splice( index , 1 );
			}
			
			choiceTxt = "";
			for ( var i:int = 0; i < choicedAry.length; i++ )
			{
				if ( choicedAry[ i ] != null )
				{
					choiceTxt += choiceSigns[ getIndex( choicedAry[ i ].name ) ];
				}
			}
			if ( choiceTf )
				choiceTf.text = choiceTxt;
		}
		
		// ---------------------------------- Public Methods ---------------------------------- //
		
		/**
		 * check plugin
		 * @return Boolean
		 */
		override public function check():Boolean
		{
			if ( choiceTxt != answer[ 0 ] )
			{
				if ( choiceTxt != '' )
					Console.log( '答案错误: ' + choiceTxt + ' - ' + answer[ 0 ], this, 1 );
				return false;
			}
			return true;
		}
		
		/**
		 * reset plugin
		 */
		override public function reset( all:Boolean = true ):void
		{
			super.reset( all );
			choiceTxt = "";
			choicedAry = [];
			if ( choiceTf )
				choiceTf.text = "";
		}
		
		/**
		 * get score
		 */
		override public function get score():Number
		{
			return check() ? tScore : 0;
		}
		
		override public function markup( mcs:Array = null ):void
		{
			if ( mcs != null && choiceTf != null )
			{
				markups.length = 0;
				var mark:Mark;
				var rect:Rectangle = choiceTf.getRect( skin );
				mark = new Mark();
				mark.x = rect.x;
				mark.y = rect.y;
				mark.width = rect.width;
				mark.height = rect.height;
				mark.position = Mark.LEFT;
				mark.type = check() ? 0 : 1;
				markups.push( mark );
			}
			super.markup( mcs );
		}
		
		/**
		 * 備份單選題
		 * @return
		 */
		override public function backup():Object
		{
			var bu:Object = {};
			bu.index = index;
			bu.choiceTxt = choiceTxt;
			var array:Array = [];
			for ( var i:int = 0; i < choicedAry.length; i++ )
			{
				array[ i ] = getIndex( choicedAry[ i ].name );
			}
			bu.choicedAry = array;
			return bu;
		}
		
		/**
		 *
		 * @param	value
		 */
		override public function recover( value:Object ):void
		{
			if ( value == null )
				return;
			if ( value.choiceTxt != undefined )
			{
				choiceTxt = value.choiceTxt;
				if ( choiceTf )
					choiceTf.text = choiceTxt;
			}
			if ( value.choicedAry != undefined )
			{
				var array:Array = value.choicedAry;
				var mc:MovieClip;
				for ( var i:int = 0; i < array.length; i++ )
				{
					mc = choices[ array[ i ] ];
					choicedAry.push( mc );
					mc.gotoAndStop(3);
				}
			}
		}
	}

}