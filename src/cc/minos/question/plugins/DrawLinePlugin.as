package cc.minos.question.plugins
{
	import cc.minos.question.extensions.Mark;
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.events.Event;
	import flash.geom.Rectangle;
	
	/**
	 * ...
	 * 画线
	 * groups: [[0,1,2,3],[4,5,6],[7,8]]
	 * answer: [[4],[5],[6],[6],[7],[7,8],[8]]
	 * @author Minos
	 */
	public class DrawLinePlugin extends ChoicePlugin
	{
		//API
		public static const API:Number = 2.1;
		
		//当前点击对象
		protected var currentChoice:MovieClip;
		//分组
		protected var groups:Array;
		//线条
		protected var lines:Shape;
		//储存画线
		protected var draweds:Array;
		//单线
		protected var isSingle:Boolean = true;
		
		/**
		 * 构造
		 */
		public function DrawLinePlugin()
		{
			this.propName = "DrawLinePlugin";
			//this.acronym = "line";
		}
		
		/**
		 * 初始
		 */
		override protected function init():void
		{
			super.init();
			draweds = [];
			groups = getData( "groups" ) as Array;
			var s:String = getData( "single" ) ? String( getData( "single" ) ) : String( getData( "isSingle" ) )
			isSingle = s.toUpperCase() == "TRUE" ? true : false;
			lines = new Shape();
			skin.addChild( lines );
		}
		
		/**
		 * 更新状态
		 * @param	mc
		 */
		override protected function update( mc:MovieClip = null ):void
		{
			super.update( mc );
			var clickChoice:MovieClip = mc;
			var index:int = getIndex( clickChoice.name );
			if ( !currentChoice )
			{
				currentChoice = clickChoice;
				choicedAry[ index ] = clickChoice;
				return;
			}
			
			var a:int = getGroup( currentChoice ), b:int = getGroup( clickChoice );
			if ( Math.abs( a - b ) != 1 )
			{
				a = getIndex( currentChoice.name ), b = getIndex( clickChoice.name );
				clearLineByIndex( b );
				if ( a != b )
					clearLineByIndex( a );
				currentChoice = null;
				return;
			}
			
			a = getIndex( currentChoice.name ), b = getIndex( clickChoice.name );
			if ( !draweds[ Math.min( a, b ) ] )
				draweds[ Math.min( a, b ) ] = [];
			var _ary:Array = draweds[ Math.min( a, b ) ];
			if ( _ary.indexOf( Math.max( a, b ) ) == -1 )
			{
				if ( isSingle )
				{
					var groups:Array = groups[ getGroup( choices[ Math.min( a, b ) ] ) ];
					for ( var i:Object in groups )
					{
						var child:Array = draweds[ groups[ i ] ];
						if ( child && child[ 0 ] == Math.max( a, b ) )
							child.splice( 0, 1 );
					}
					draweds[ Math.min( a, b ) ][ 0 ] = Math.max( a, b );
				}
				else
					_ary.push( Math.max( a, b ) );
			}
			updateLines();
			currentChoice = null;
		}
		
		/**
		 * 更新线条
		 */
		protected function updateLines():void
		{
			var temp:Array = [];
			lines.graphics.clear();
			lines.graphics.lineStyle( 2 );
			//draw line
			for ( var i:Object in draweds )
			{
				if ( !draweds[ i ] )
					continue;
				for each ( var j:Object in draweds[ i ] )
				{
					lines.graphics.moveTo( choices[ i ].x, choices[ i ].y );
					lines.graphics.lineTo( choices[ j ].x, choices[ j ].y );
					//add to choiceed
					choicedAry[ i ] = choices[ i ], choicedAry[ j ] = choices[ j ];
					temp[ i ] = i, temp[ j ] = j;
				}
			}
			//set style
			for ( i in choicedAry )
			{
				if ( temp[ i ] == i )
					choicedAry[ i ].gotoAndStop( 3 );
				else if ( choicedAry[ i ] )
				{
					choicedAry[ i ].gotoAndStop( 1 );
					choicedAry[ i ] = null;
				}
			}
		}
		
		/**
		 * 清除线条
		 * @param	index
		 */
		protected function clearLineByIndex( index:int, isUpdate:Boolean = true ):void
		{
			var child:Array = draweds[ index ];
			if ( child != null && child.toString() != "" )
				draweds[ index ] = [];
			else
			{
				for ( var i:Object in draweds )
				{
					child = draweds[ i ];
					if ( child && child.indexOf( index ) != -1 )
						child.splice( child.indexOf( index ), 1 );
				}
			}
			if ( isUpdate )
				updateLines();
		}
		
		/**
		 * 获取分组
		 * @param	mc
		 * @return
		 */
		protected function getGroup( mc:MovieClip = null ):int
		{
			var index:int = getIndex( mc.name );
			var group:int = -1;
			for ( var i:int = 0, len:int = groups.length; i < len; i++ )
			{
				if ( groups[ i ].indexOf( index ) != -1 )
				{
					group = i;
					break;
				}
			}
			return group;
		}
		
		/**
		 * 检查
		 * @return
		 */
		override public function check():Boolean
		{
			for ( var i:Object in answer )
			{
				var d:Object = draweds[ i ];
				if ( d is Array )
					( d as Array ).sort();
				var a:Object = answer[ i ];
				if ( a is Array )
					( a as Array ).sort();
				
				if ( !d || d.toString() != a.toString() )
					return false;
			}
			return true;
		}
		
		/**
		 * 得分
		 */
		override public function get score():Number
		{
			_score = 0;
			if ( checkAll )
				return check() ? tScore : 0;
			//
			for ( var i:int = 0, len:int = answer.length; i < len; i++ )
			{
				if ( draweds[ i ] && answer[ i ].toString() == draweds[ i ].toString() )
					_score += tScore / len;
			}
			//
			return _score;
		}
		
		/**
		 * 重置
		 */
		override public function reset( all:Boolean = true ):void
		{
			super.reset( all );
			draweds = [];
			choicedAry = [];
			lines.graphics.clear();
		}
		
		/**
		 * 備份連線題
		 * 保存draweds數據
		 * @return
		 */
		override public function backup():Object
		{
			return { draweds: draweds, index: index };
		}
		
		/**
		 * 恢復
		 * 還原draweds然後更新線
		 * @param	value
		 */
		override public function recover( value:Object ):void
		{
			if ( value == null )
				return;
			if ( value.draweds != undefined )
			{
				draweds = value.draweds;
				updateLines();
			}
		}
		
		/**
		 * 標記連線題
		 * 位置為第一組的左上角
		 * @param	mcs
		 */
		override public function markup( mcs:Array = null ):void
		{
			if ( mcs != null )
			{
				var mark:Mark;
				var rect:Rectangle;
				for ( var i:int = 0, len:int = answer.length; i < len; i++ )
				{
					rect = choices[i].getRect( skin );
					mark = new Mark();
					mark.width = rect.width;
					mark.height = rect.height;
					mark.x = rect.x;
					mark.y = rect.y;
					mark.position = Mark.LEFT;
					if ( draweds[ i ] && answer[ i ].toString() == draweds[ i ].toString() )
					{
						mark.type = 0;
					}
					else
					{
						mark.type = 1;
					}
					markups.push( mark );
				}
			}
			super.markup( mcs );
		}
	}

}