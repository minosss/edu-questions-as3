package cc.minos.question.plugins
{
	import mx.utils.StringUtil;
	
	/**
	 * ...
	 * 无序填空
	 * @author Minos
	 */
	public class OrderFillInPlugin extends FillInPlugin
	{
		public static const NAME:String = "[OrderFillInPlugin]";
		public static const API:Number = 2.1;
		//无序
		protected var order:Boolean = false;
		
		/**
		 * 构造
		 */
		public function OrderFillInPlugin()
		{
			super();
			this.propName = "OrderFillInPlugin";
			optionName = "a_$_txt";
		}
		
		/**
		 * 初始
		 */
		override protected function init():void
		{
			super.init();
			order = String( getData( 'order' ) ).toUpperCase() == "TRUE" ? true : false;
		}
		
		/**
		 * 检查题目
		 * 检查的时候重新获取答案，因为为了唯一答案正确后会删除。
		 * @version 1118
		 * @return
		 */
		override public function check():Boolean
		{
			answer = getAnswer();
			return super.check();
		}
		
		/**
		 * 检查答案
		 * @param	txt
		 * @param	ans
		 * @return
		 */
		override protected function _check( i:int ):Boolean
		{
			var txt:String = tfs[ i ].text;
			var ans:Object = answer[ i ];
			txt = StringUtil.trim( txt );
			if ( order )
			{
				if ( answer.indexOf( txt ) != -1 )
				{
					answer.splice( answer.indexOf( txt ), 1 );
					return true;
				}
				return false;
			}
			return super._check( i );
		}
		
		/**
		 * 重置
		 */
		override public function reset( all:Boolean = true ):void
		{
			super.reset( all );
			answer = getAnswer();
		}
	
	}

}