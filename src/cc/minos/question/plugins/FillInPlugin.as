package cc.minos.question.plugins
{
	import cc.minos.console.Console;
	import cc.minos.question.extensions.Mark;
	import cc.minos.utils.StringUtil;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import flash.utils.ByteArray;
	
	/**
	 * ...
	 * FillIn 填空
	 * 0908 更新一个空格多个答案
	 * @author Minos
	 */
	
	public class FillInPlugin extends QuestionPlugin
	{
		public static const NAME:String = "[FillInPlugin]";
		public static const API:Number = 2.1;
		// textfield array
		protected var tfs:Array;
		
		public function FillInPlugin()
		{
			this.propName = "FillInPlugin";
			//this.acronym = "fillin";
			optionName = "a_$_txt";
		}
		
		// ----- protected methods ----- //
		
		override protected function init():void
		{
			super.init();
			tfs = [];
			var index:int;
			for each ( var o:Object in skin )
			{
				if ( o && o is TextField && o.name.charAt( 0 ) == "a" )
				{
					index = int( StringUtil.substringBySign( o.name ) );
					o.text = ""
					o.tabIndex = index;
					tfs[ index ] = o;
				}
			}
			options = tfs.length;
		
			//markup
		}
		
		// Public Methods //
		
		override public function activate():void
		{
			super.activate()
			for ( var i:int = 0, len:int = tfs.length; i < len; i++ )
			{
				if ( tfs[ i ] )
					tfs[ i ].mouseEnabled = true;
			}
		}
		
		override public function cancel():void
		{
			super.cancel();
			for ( var i:int = 0, len:int = tfs.length; i < len; i++ )
			{
				if ( tfs[ i ] )
					tfs[ i ].mouseEnabled = false;
			}
		}
		
		/**
		 *
		 * @return
		 */
		override public function check():Boolean
		{
			for ( var i:int = 0, len:int = tfs.length; i < len; i++ )
			{
				if ( tfs[ i ] && !_check( i ) )
				{
					return false;
				}
			}
			return true;
		}
		
		/**
		 * 获取得分
		 */
		override public function get score():Number
		{
			_score = 0;
			if ( checkAll )
				return check() ? tScore : 0;
			
			for ( var i:int = 0, len:int = tfs.length; i < len; i++ )
			{
				if ( tfs[ i ] && _check( i ) )
				{
					_score += ( tScore / len );
				}
				else if ( tfs[ i ].text != '' )
				{
					Console.log( '答案错误: ' + answer[ i ] + ' - ' + tfs[ i ].text, this, 1 );
				}
			}
			return _score;
		}
		
		/**
		 * 0908 根据答案类型检查
		 * @param	i
		 * @return
		 */
		protected function _check( i:int ):Boolean
		{
			var txt:String = tfs[ i ].text;
			var ans:Object = answer[ i ];
			var b:Boolean = true;
			txt = StringUtil.trim( txt );
			//fill many
			if ( ans == null )
				b = false;
			else if ( ans is Array )
			{
				if (( ans as Array ).indexOf( txt ) == -1 )
					b = false;
			}
			else
			{
				if ( StringUtil.trim( ans.toString() ) != txt )
					b = false;
			}
			return b;
		}
		
		/**
		 * 重置
		 */
		override public function reset( all:Boolean = true ):void
		{
			super.reset( all );
			for ( var i:int = 0, len:int = tfs.length; i < len; i++ )
			{
				if ( tfs[ i ] )
				{
					tfs[ i ].text = "";
				}
			}
		}
		
		protected function getTextFiledByIndex( idx:int = 0 ):TextField
		{
			return ( getChild( optionName.replace( "$", idx ) ) as TextField );
		}
		
		override public function markup( mcs:Array = null ):void
		{
			if ( mcs != null )
			{
				markups.length = 0;
				var mark:Mark;
				var rect:Rectangle;
				for ( var i:int = 0; i < tfs.length; i++ )
				{
					if ( checkAll )
						i = tfs.length - 1;
					if ( tfs[ i ] != null )
					{
						rect = tfs[i].getRect( skin );
						mark = new Mark();
						mark.x = rect.x;
						mark.y = rect.y;
						mark.width = rect.width;
						mark.height = rect.height;
						mark.position = Mark.RIGHT;
						mark.type = _check( i ) ? 0 : 1;
						markups.push( mark );
					}
				}
			}
			super.markup( mcs );
		}
		
		override public function backup():Object
		{
			var bu:Object = {};
			bu.index = index;
			var fills:Array = [];
			for ( var i:int = 0; i < tfs.length; i++ )
			{
				if ( tfs[ i ] != null )
					fills.push( tfs[ i ].text );
				else
					fills.push( "" );
			}
			bu.fills = fills;
			return bu;
		}
		
		override public function recover( value:Object ):void
		{
			if ( value == null )
				return;
			if ( value.fills != undefined )
			{
				var fills:Array = value.fills;
				for ( var i:int = 0; i < fills.length; i++ )
				{
					if ( tfs[ i ] != null )
						tfs[ i ].text = fills[ i ];
				}
			}
		}
	}

}