package cc.minos.question.extensions
{
	
	/**
	 * 導出數據
	 * @author Minos
	 */
	public interface IPrintData
	{
		function print():Object;
	}

}