package cc.minos.question.extensions
{
	
	/**
	 * ...
	 * @author Minos
	 */
	public class Mark
	{
		public static const LEFT_DOWN:uint = 1;
		public static const DOWN:uint = 2;
		public static const RIGHT_DOWN:uint = 3;
		
		public static const LEFT:uint = 4;
		public static const CENTER:uint = 5;
		public static const RIGHT:uint = 6;
		
		public static const LEFT_UP:uint = 7;
		public static const UP:uint = 8;
		public static const RIGHT_UP:uint = 9;
		
		public var x:int = 0;
		public var y:int = 0;
		public var width:uint = 0;
		public var height:uint = 0;
		public var type:int = 0;
		public var position:uint = CENTER;
		
		public function Mark()
		{
		}
	
	}

}