package cc.minos.question.extensions
{
	
	/**
	 * 數據備份
	 * @author Minos
	 */
	public interface IRecovery
	{
		function backup():Object;
		function recover( value:Object ):void;
	}

}