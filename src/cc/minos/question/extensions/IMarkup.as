package cc.minos.question.extensions
{
	import flash.utils.ByteArray;
	
	/**
	 * ...
	 * @author Minos
	 */
	public interface IMarkup
	{
		function markup( mcs:Array = null ):void;
	}

}