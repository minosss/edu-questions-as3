**Deprecated**

UPDATE
======

2013/08/28
  * +添加[Irecovery记录, IPrintData导出, IContinue继续]方法
  * =需求增加Question类启用继承
  * +FillInPlugin tabIndex

2013/08/07
  * +准备添加记录答案功能[ IRecovery ]

2012/12/20
  * +添加拖动句子多答案功能

2012/12/04
  * =修复拖动句子多文本错误 946 & 1169

2012/10/09
  * =修复拖动题判断问题

2012/10/08
  * +拖动题匹配多个答案 
  * +拖动句子插入功能

2011/10/08
  * =修复分数有小数点问题（Testing 809 & Diagnoses 827）

2011/09/28
  * +添加题目类型 OrderFillInPlugin 无序填空题 

2011/09/08
  * +支持连线题


QUESTION
========
教育教学题目类，各种类型的题型已插件形式添加，用xml配置搭配swf素材。 <br/>
  * DragPlugin 拖动 
  * DrawLinePlugin 连线 
  * SingleChoicePlugin 单选
  * MultiChoicePlugin 多选 
  * SentencePlugin 组句子 
  * SoundPlugin 声音 
  * FillInPlugin 填空 
  * OrderFillInPlugin 无序填空 
  * JudgmentPlugin  判断 
